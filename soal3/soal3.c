#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <wait.h>
#include <dirent.h>
#include <stdio.h>
#include <sys/stat.h>
#include <pwd.h>
#include <grp.h>

char keyMkdir[] = "/bin/mkdir";
char keyUnzip[] = "/usr/bin/unzip";
char keyRemove[] = "/usr/bin/rm";
char keyCopy[] = "/usr/bin/cp";
char keyMove[] = "/usr/bin/mv";
char keyTouch[] = "/usr/bin/touch";

void createProcess(char *str, char *argv[])
{
  pid_t child_id;
  child_id = fork();
  int status;

  if (child_id == 0)
  {
    execv(str, argv);
  }
  else
  {
    while (wait(&status) > 0)
      ;
  }
}

void createFolderDarat()
{
  char *argv[] = {"mkdir", "darat", NULL};
  createProcess(keyMkdir, argv);
}

void createFolderAir()
{
  char *argv[] = {"mkdir", "air", NULL};
  createProcess(keyMkdir, argv);
}

void unzipAnimalZip()
{
  char *argv[] = {"unzip", "-qo", "animal.zip", NULL};
  createProcess(keyUnzip, argv);
}

void moveFile(char *folder, char *source)
{
  char coreFolder[] = "/home/ramammurshal/modul2/";
  strcat(coreFolder, folder);
  char *argv[] = {"mv", source, coreFolder, NULL};
  createProcess(keyMove, argv);
}

void removeFile(char *source)
{
  char *argv[] = {"rm", source, NULL};
  createProcess(keyRemove, argv);
}

void moveAnimalFile()
{
  DIR *dp;
  struct dirent *ep;
  pid_t childs;
  int status = 0;

  childs = fork();

  if (childs == 0)
  {
    dp = opendir("animal");
    if (dp != NULL)
    {
      while ((ep = readdir(dp)))
      {
        if (strcmp(ep->d_name, ".") != 0 && strcmp(ep->d_name, "..") != 0)
        {
          char *containDarat = strstr(ep->d_name, "darat");

          char source[100] = "animal/";
          strcat(source, ep->d_name);

          if (containDarat)
          {
            moveFile("darat", source);
          }
        }
      }
      (void)closedir(dp);
    }
  }
  else
  {
    while (wait(&status) > 0)
      ;
    sleep(3);
    dp = opendir("animal");
    if (dp != NULL)
    {
      while ((ep = readdir(dp)))
      {
        if (strcmp(ep->d_name, ".") != 0 && strcmp(ep->d_name, "..") != 0)
        {
          char *containAir = strstr(ep->d_name, "air");

          char source[100] = "animal/";
          strcat(source, ep->d_name);

          if (containAir)
          {
            moveFile("air", source);
          }
          else
          {
            removeFile(source);
          }
        }
      }
      (void)closedir(dp);
    }
  }
}

void removeBirdFromDarat()
{
  DIR *dp;
  struct dirent *ep;

  dp = opendir("darat");

  if (dp != NULL)
  {
    while ((ep = readdir(dp)))
    {
      if (strcmp(ep->d_name, ".") != 0 && strcmp(ep->d_name, "..") != 0)
      {
        char *containBird = strstr(ep->d_name, "bird");

        char source[100] = "darat/";
        strcat(source, ep->d_name);

        if (containBird)
        {
          removeFile(source);
        }
      }
    }

    (void)closedir(dp);
  }
  else
  {
    exit(0);
  }
}

void createTxt(char *where)
{
  char *argv[] = {"touch", where, NULL};
  createProcess(keyTouch, argv);
}

void writeTxt(char *folder, char *file)
{
  // dir
  DIR *dp;
  struct dirent *ep;
  dp = opendir(folder);

  // file
  FILE *f = fopen(file, "w");
  if (f == NULL)
  {
    exit(1);
  }
  struct stat info;
  struct stat fs;
  int r;
  int s;

  if (dp != NULL)
  {
    while ((ep = readdir(dp)))
    {
      if (strcmp(ep->d_name, ".") != 0 && strcmp(ep->d_name, "..") != 0)
      {
        char temp[999];
        char temp2[999];
        if (folder == "darat")
        {
          strcpy(temp, "darat/");
          strcpy(temp2, "darat/");
        }
        else
        {
          strcpy(temp, "air/");
          strcpy(temp2, "air/");
        }
        r = stat(strcat(temp, ep->d_name), &info);
        s = stat(strcat(temp2, ep->d_name), &fs);

        if (r == -1 || s == -1)
        {
          fprintf(stderr, "File error satu \n");
          exit(1);
        }

        struct passwd *pw = getpwuid(info.st_uid);
        if (pw == 0)
        {
          fprintf(stderr, "File error dua \n");
          exit(1);
        }
        char *userFile = pw->pw_name;
        fprintf(f, "%s_", userFile);

        if (fs.st_mode & S_IRUSR)
        {
          fprintf(f, "r");
        }
        else
        {
          fprintf(f, "-");
        }

        if (fs.st_mode & S_IWUSR)
        {
          fprintf(f, "w");
        }
        else
        {
          fprintf(f, "-");
        }

        if (fs.st_mode & S_IXUSR)
        {
          fprintf(f, "x");
        }
        else
        {
          fprintf(f, "-");
        }

        char *fileName = ep->d_name;
        fprintf(f, "_%s \n", fileName);
      }
    }

    fclose(f);
    (void)closedir(dp);
  }
  else
  {
    exit(0);
  }
}

int main()
{
  pid_t child_id;
  child_id = fork();
  int status;

  if (child_id < 0)
  {
    exit(EXIT_FAILURE);
  }

  if (child_id != 0)
  {
    createFolderDarat();
  }
  else
  {
    while (wait(&status) > 0)
      ;
    sleep(3);
    createFolderAir();
    unzipAnimalZip();
    moveAnimalFile();
    removeBirdFromDarat();
    createTxt("darat/list.txt");
    writeTxt("darat", "darat/list.txt");
    createTxt("air/list.txt");
    writeTxt("air", "air/list.txt");
  }
}

