# Soal Shift Sistem Operasi Modul-2 ITA05
Anggota kelompok:
- Sarah Hanifah Pontoh - 5027201006
- Rama Muhammad Murshal - 5027201041
- Muhammad Rifqi Fernanda - 5027201050

# Daftar Isi
- [Soal 1](#soal-1)
- [Soal 2](#soal-2)
- [Soal 3](#soal-3)

## Soal 1

### Ringkasan soal
- Program gacha untuk menjalankan gacha weapons dan characters secara bergantian. Program akan mendownload file characters dan file weapons, lalu program akan mengekstrak kedua file tersebut. File tersebut akan digunakan sebagai database untuk melakukan gacha item characters dan weapons. Kemudian akan dibuat sebuah folder dengan nama “gacha_gacha” sebagai working directory. Seluruh hasil gacha akan berada di dalam folder tersebut.
- Setiap kali gacha, item characters dan item weapon akan selalu bergantian diambil datanya dari database. Maka untuk setiap kali jumlah-gacha nya bernilai genap akan dilakukan gacha item weapons, jika bernilai ganjil maka item characters. Lalu untuk setiap kali jumlah-gacha nya mod 10, maka akan dibuat sebuah file baru (.txt) dan output hasil gacha selanjutnya akan berada di dalam file baru tersebut. Dan setiap kali jumlah-gacha nya mod 90, maka akan dibuat sebuah folder baru dan file (.txt) selanjutnya akan berada didalam folder baru tersebut.  
- Pada game tersebut, untuk melakukan gacha item kita harus menggunakan alat tukar yang dinamakan primogems. Satu kali gacha item akan menghabiskan primogems sebanyak 160 primogems. Karena mas Refadi ingin agar hasil simulasi gacha nya terlihat banyak, maka pada program, primogems di awal di-define sebanyak 79000 primogems. Setiap kali gacha, ada 2 properties yang akan diambil dari database, yaitu name dan rarity. Lalu Outpukan hasil gacha nya ke dalam file (.txt) dengan format hasil gacha {jumlah-gacha}_[tipe-item]_{rarity}_{name}_{sisa-primogems}. 
- Proses untuk melakukan gacha item akan dimulai bertepatan dengan anniversary pertama kali mas Refadi bermain bengshin impek, yaitu pada 30 Maret jam 04:44.  Kemudian agar hasil gacha nya tidak dilihat oleh teman kos nya, maka 3 jam setelah anniversary tersebut semua isi di folder gacha_gacha akan di zip dengan nama not_safe_for_wibu dengan dipassword "satuduatiga", lalu semua folder akan di delete sehingga hanya menyisakan file (.zip)

### Pembahasan 
```c
void download(char *db, char *file)
{
  char *argv[] = {"wget", "--no-check-certificate", "-q", db, "-O", file, NULL}; 
  createProcess(keyWget, argv);
}

void unzip(char *file)
{
  char *argv[] = {"unzip", "-q", "-n", file, NULL}; 
  createProcess(keyUnzip, argv);
}

void createFolderGacha(char *dir)
{
  DIR *checkDir = opendir(dir);
  if (checkDir)
  { //check if gacha_gacha exists
    closedir(checkDir);
  }
  else
  {
    char *argv[] = {"mkdir", dir, NULL}; 
    createProcess(keyMkdir, argv);
  }
}
```
Menggunakan function `download` untuk mendownload character dan weapon dari link menggunakan `wget`. `unzip` dilakukan untuk mengekstrak zip folder, dan  `createFolderGacha` menggunakan `mkdir` untuk membuat directory gacha_gacha. Ketiga fungsi ini akan dipanggil di `main`.


```c
while(current_primogems > cost)
  {
    current_primogems-=cost;
    if((gacha_number%90-1)==0)
    {
      if(gacha_number > 1) chdir("..");
      snprintf(foldername, 100, "total_gatcha_%d", gacha_number);

      char *argv[] = {"mkdir", "-p", foldername, NULL};
      createProcess(keyMkdir, argv);
      chdir(foldername);
    }

    if((gacha_number%10-1)==0 || gacha_number==1)
    {
      sleep(1);
      time_t t1 = time(NULL);
      struct tm* p1 = localtime(&t1);

      strftime(waktu, 30, "%H:%M:%S", p1);
      snprintf(filename, 100, "%s_gatcha_%d.txt", waktu, gacha_number);
    }

    if(gacha_number%2==0)
    {
      fptr = fopen(filename, "a+");
      snprintf(message, 100, "%d_weapon_%s_%d\n", gacha_number, fetch("../../weapons"), current_primogems);
      fputs(message, fptr);
      fclose(fptr);
    }
    else
    {
      fptr = fopen(filename, "a+");
      snprintf(message, 100, "%d_character_%s_%d\n", gacha_number, fetch("../../characters"), current_primogems);
      fputs(message, fptr);
      fclose(fptr);
    }
```

Menggunakan while loop selama primogems masih tersedia. Jika gacha mod 90, maka akan dibuat folder baru menggunakan `mkdir`. Jika jumlah gacha mod 10, maka akan dibuat file baru. Jika jumlah gacha genap, program akan mengambil data dari weapon dan jika ganjil dari characters.

```c
char *fetch(char *basePath)
{
  FILE *fp;
	char buffer[4096];
  char *data = malloc (sizeof (char) * 1024);
	struct json_object *parsed_json;
	struct json_object *name;
	struct json_object *rarity;

  char filename[100];
  snprintf(filename, 100, "%s/%s", basePath, pickItem(basePath));


  fp = fopen(basePath,"r");
	fread(buffer, 4096, 1, fp);
	fclose(fp);

	parsed_json = json_tokener_parse(buffer);

	json_object_object_get_ex(parsed_json, "name", &name);
	json_object_object_get_ex(parsed_json, "rarity", &rarity);

  snprintf(data, 1024, "%d_%s", json_object_get_int(rarity), json_object_get_string(name));
  return data;
}
```

Function `fetch` dilakukan unutk mengambil data name dan rarity dari sebuah item. Item dipilih secara random (namun sesuai basepath weapon/characters) menggunakan fungsi `pickItem`.

```c
char *pickItem(char *basePath)
{
  DIR *d;
  struct dirent *dir;
  d = opendir(basePath);

  //count how many json files
  int files = countItem(basePath);
  //generate random number
  int random = rand()%(files-1) + 1;

  if (d)
  {
    int i=0;
    while ((dir = readdir(d)) != NULL)
    {
      if(i>=random-1) 
      {
        return dir->d_name;
      }
      i++;
    }
  }
  
}
```

Karena soal meminta untuk menzip file setelah gacha agar tidak terlihat, maka dibuat function `private`.

```c
void private()
{
  
  char *argv[] = {"zip", "--password", "satuduatiga", "-r", "not_safe_for_wibu.zip", "gacha_gacha", NULL};
  createProcess(keyZip, argv);

  char *argv2[] = {"rm", "-r", "gacha_gacha", "characters", "weapons", NULL};
  createProcess(keyRmove, argv2);

  char *argv3[] = {"rm", "characters.zip", "weapons.zip", NULL};
  createProcess(keyRmove, argv3);

}
```
## Kendala
Program di run dengan command berikut

![command](./images/soal1.png)

Untuk nomor 1 ini, kami masih mengalami error dimana hasil gacha tidak dapat ditampikan di folder `gacha_gacha`.

![command](./images/soal1_error.png)

## Soal 2
Japrun bekerja di sebuah perusahaan dibidang review industri perfilman, karena kondisi saat ini sedang pandemi Covid-19, dia mendapatkan sebuah proyek untuk mencari drama korea yang tayang dan sedang ramai di Layanan Streaming Film untuk diberi review. Japrun sudah mendapatkan beberapa foto-foto poster serial dalam bentuk zip untuk diberikan review, tetapi didalam zip tersebut banyak sekali poster drama korea dan dia harus memisahkan poster-poster drama korea tersebut tergantung dengan kategorinya. Japrun merasa kesulitan untuk melakukan pekerjaannya secara manual, kamu sebagai programmer diminta Japrun untuk menyelesaikan pekerjaannya.

### Keterangan Soal 2
- File zip berada dalam drive modul shift ini bernama drakor.zip
- File yang penting hanyalah berbentuk .png
- Setiap foto poster disimpan sebagai nama foto dengan format [nama]:[tahun rilis]:[kategori]. Jika terdapat lebih dari satu drama dalam poster, dipisahkan menggunakan underscore(_).
- Tidak boleh menggunakan fungsi system(), mkdir(), dan rename() yang tersedia di bahasa C.
- Gunakan bahasa pemrograman C (Tidak boleh yang lain).
- Folder shift2, drakor, dan kategori dibuatkan oleh program (Tidak Manual).
- [user] menyesuaikan nama user linux di os anda.

### Penjabaran Umum Pengerjaan Soal 2
Program `soal2.c` yang kami buat disimpan pada home directory os linux kita. Di awal kode program, kita mendefinisikan library-library yang dibutuhkan untuk mengembangkan program sebagai berikut:

```c
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <wait.h>
#include <dirent.h>
#include <stdio.h>
#include <sys/stat.h>
#include <pwd.h>
#include <grp.h>
```

Setelah itu, kami membuat beberapa string global yang bakal digunakan untuk melakukan perintah `execv()` dalam bahasa C. Kode tersebut sesuai dengan:

```c
char keyMkdir[] = "/bin/mkdir";
char keyUnzip[] = "/usr/bin/unzip";
char keyRemove[] = "/usr/bin/rm";
char keyCopy[] = "/usr/bin/cp";
char keyMove[] = "/usr/bin/mv";
char keyTouch[] = "/usr/bin/touch";
```

Kemudian, secara garis besar, program yang kami buat terbagi menjadi fungsi-fungsi spesifik yang akan mengerjakan tugas tertentu dan sisa dipanggil pada fungsi main. Isi dari fungsi main tersebut adalah sesuai berikut:

```c
int main()
{
  createParentFolder();
  extractZip();
  removeDir();
  createFolderDrakorByCategory();
  moveFileByCategory();
  getListFolderDrakor();
}
```

Kemudian, untuk dapat menjalankan perintah `execv()` secara berulang kali, kita membuat sebuah fungsi yang bertugas untuk membuat sebuah proses (forking). Fungsi ini kemudian akan dipanggil secara terus-menerus pada setiap fungsi yang ada di dalam `main()`. Fungsi `createProcess()` adalah sebagai berikut:

```c
void createProcess(char *str, char *argv[])
{
  pid_t child_id;
  child_id = fork();
  int status;

  if (child_id == 0)
  {
    execv(str, argv);
  }
  else
  {
    while (wait(&status) > 0)
      ;
  }
}
```

### Soal 2.a
Hal pertama yang perlu dilakukan oleh program adalah mengextract zip yang diberikan ke dalam folder “/home/[user]/shift2/drakor”. Karena atasan Japrun teledor, dalam zip tersebut bisa berisi folder-folder yang tidak penting, maka program harus bisa membedakan file dan folder sehingga dapat memproses file yang seharusnya dikerjakan dan menghapus folder-folder yang tidak dibutuhkan.

### Pemabahasan Soal 2.a
Hal pertama yang kami lakukan adalah membuat folder drakor yang ada dalam folder shift2 yang ada dalam home directory linux kita. Hal ini dilakukan pada fungsi `createParentFolder()` sebagai berikut:

```c
void createParentFolder()
{
  char *argv[] = {"mkdir", "-p", "/home/ramammurshal/shift2/drakor/", NULL};
  createProcess(keyMkdir, argv);
}
```

Kemudian, kita melakukan extract zip drakor (disimpan pada `/home/[user]/modul2`) yang dilaksanakan oleh fungsi `extractZip()` sebagai berikut:

```c
void extractZip()
{
  char *argv[] = {"unzip", "-qo", "/home/ramammurshal/modul2/drakor.zip", "-d", "/home/ramammurshal/shift2/drakor", NULL};
  createProcess(keyUnzip, argv);
}
```

Lalu, kita menghapus folder hasil extract zip yang tidak berguna. Ini dilaksanakan oleh fungsi `removeDir()` sesuai kode berikut:

```c
void removeDir()
{
  char *argv[] = {"rm", "-r", "shift2/drakor/coding/", "shift2/drakor/song/", "shift2/drakor/trash", NULL};
  createProcess(keyRemove, argv);
}
```

Hasil dari pengerjaan 2.a adalah sebagai berikut:

![Image hasil 2.a](./images/2a.PNG)

### Soal 2.b
Poster drama korea perlu dikategorikan sesuai jenisnya, maka program harus membuat folder untuk setiap jenis drama korea yang ada dalam zip. Karena kamu tidak mungkin memeriksa satu-persatu manual, maka program harus membuatkan folder-folder yang dibutuhkan sesuai dengan isi zip. Contoh: Jenis drama korea romance akan disimpan dalam “/drakor/romance”, jenis drama korea action akan disimpan dalam “/drakor/action” , dan seterusnya.

### Pembahasan Soal 2.b
Dalam mengerjakan soal 2b, kita membuat sebuah fungsi `createFolderDrakorByCategory()`. Di dalam fungsi tersebut, kita menggunakan template listing file yang ada di github untuk melihat semua isi file di dalam folder hasil extract zip. Untuk setiap folder yang belum terdapat jenis/kategorinya, kita menjalankan perintah `execv` yang akan menjalankan perintah `mkdir`. Untuk memisahkan setiap potongan nama file, kita menggunakan fungsi `strtok`. Fungsi `createFolderDrakorByCategory()` sebagai berikut:

```c
void createFolderDrakorByCategory()
{
  DIR *dp;
  struct dirent *ep;

  dp = opendir("shift2/drakor/");

  if (dp != NULL)
  {
    while ((ep = readdir(dp)))
    {
      if (strcmp(ep->d_name, ".") != 0 && strcmp(ep->d_name, "..") != 0)
      {
        int i = 0;
        char arr[5][50] = {};
        char *token = strtok(ep->d_name, ";");

        while (token != NULL)
        {
          strcpy(arr[i], token);
          i++;
          token = strtok(NULL, ";");
        }

        if (i == 3)
        {
          char *token3 = strtok(arr[2], ".");
          char corePath[100] = "/home/ramammurshal/shift2/drakor/";
          strcat(corePath, token3);
          char *createFolder3[] = {"mkdir", "-p", corePath, NULL};
          createProcess("/bin/mkdir", createFolder3);
        }
        else if (i == 5)
        {
          char *token53 = strtok(arr[2], "_");
          char *token55 = strtok(arr[4], ".");
          char corePath1[100] = "/home/ramammurshal/shift2/drakor/";
          char corePath2[100] = "/home/ramammurshal/shift2/drakor/";
          strcat(corePath1, token53);
          strcat(corePath2, token55);
          char *createFolder53[] = {"mkdir", "-p", corePath1, NULL};
          char *createFolder55[] = {"mkdir", "-p", corePath2, NULL};
          createProcess(keyMkdir, createFolder53);
          createProcess(keyMkdir, createFolder55);
        }
      }
    }
    (void)closedir(dp);
  }
  else
  {
    exit(0);
  }
}
```

Hasil dari pengerjaan 2.b adalah sebagai berikut:

![Image hasil 2.b](./images/2b.PNG)

### Soal 2.c
Setelah folder kategori berhasil dibuat, program akan memindahkan poster ke folder dengan kategori yang sesuai dan di rename dengan nama. Contoh: “/drakor/romance/start-up.png”.

### Soal 2.d
Karena dalam satu foto bisa terdapat lebih dari satu poster maka foto harus di pindah ke masing-masing kategori yang sesuai. Contoh: foto dengan nama “start-up;2020;romance_the-k2;2016;action.png” dipindah ke folder “/drakor/romance/start-up.png” dan “/drakor/action/the-k2.png”. (note 19/03: jika dalam satu foto ada lebih dari satu poster maka foto tersebut dicopy jadi akhirnya akan jadi 2 foto).

### Soal 2.e
Di setiap folder kategori drama korea buatlah sebuah file "data.txt" yang berisi nama dan tahun rilis semua drama korea dalam folder tersebut, jangan lupa untuk sorting list serial di file ini berdasarkan tahun rilis (Ascending). Format harus sesuai contoh dibawah ini.

![format data.txt 2e](./images/format2e.PNG)

### Pembahasan Soal 2.c, 2.d, dan 2.e
Dalam mengerjakan soal 2.c, 2.d, dan 2.e, fungsi yang kami gunakan saling terkait satu-sama lain. Beberapa fungsi tersebut adalah `moveFileByCategory()`, `getListFolderDrakor()`, `renameFileAndCreateTxtPerFolder()`, `insertionSort()`. Untuk fungsi pertama yaitu `moveFileByCategory()` berfungsi untuk memindahkan isi dari folder `/home/[user]/shift2/drakor/` yang berisi folder-folder per-kategori yang telah dibuat dan file poster drakor. Kita loop / list semua file yang ada di dalam folder tersebut, lalu kita pindahkan filenya ke folder yang sesuai. Dalam menentukan ke folder mana file harus pergi, kita menggunakan fungsi `strtok()` dengan parameter pemisah adalah `;`. Untuk file yang hasil katanya ada 3, maka kemana folder tersebut harus pergi adalah sesuai kata ke-3nya (menggunakan move). Sedangkan untuk file yang hasil pisahnya terdapat 5 kata, maka akan dipindahkan ke 2 folder. Untuk arah folder pertama sesuai dengan kata ke-3nya (menggunakan copy), dan arah folder keduanya sesuai dengan kata ke-5nya (menggunakan move). Notes bahwa kami hanya baru memindahkan filenya (belum melakukan renam terhadap file). Isi dari function `moveFileByCategory()` adalah sebagai berikut:

```c
void moveFileByCategory()
{
  DIR *dp;
  struct dirent *ep;

  dp = opendir("shift2/drakor/");

  if (dp != NULL)
  {
    while ((ep = readdir(dp)))
    {
      if (strcmp(ep->d_name, ".") != 0 && strcmp(ep->d_name, "..") != 0)
      {
        int i = 0;
        char arr[5][50] = {};
        char fileName[100];
        strcpy(fileName, ep->d_name);
        char *token = strtok(ep->d_name, ";");

        while (token != NULL)
        {
          strcpy(arr[i], token);
          i++;
          token = strtok(NULL, ";");
        }

        if (i == 3)
        {
          char *token3 = strtok(arr[2], ".");
          char corePath[100] = "/home/ramammurshal/shift2/drakor/";
          char destination[100];
          char fileSources[100];

          // copy the corePath
          strcpy(destination, corePath);
          strcpy(fileSources, corePath);

          // define destination
          strcat(destination, token3);
          strcat(destination, "/");

          // define file sources
          strcat(fileSources, fileName);

          char *moveFile3[] = {"mv", fileSources, destination, NULL};
          createProcess(keyMove, moveFile3);
        }
        else if (i == 5)
        {
          char *token53 = strtok(arr[2], "_");
          char *token55 = strtok(arr[4], ".");
          char corePath[100] = "/home/ramammurshal/shift2/drakor/";
          char destination1[100];
          char destination2[100];
          char fileSources1[100];
          char fileSources2[100];

          // copy the corePath
          strcpy(destination1, corePath);
          strcpy(destination2, corePath);
          strcpy(fileSources1, corePath);
          strcpy(fileSources2, corePath);

          // define file sources
          strcat(fileSources1, fileName);
          strcat(fileSources2, fileName);

          // define destination1
          strcat(destination1, token53);
          strcat(destination1, "/");

          // define destination2
          strcat(destination2, token55);
          strcat(destination2, "/");

          char *copyFile53[] = {"cp", fileSources1, destination1, NULL};
          createProcess(keyCopy, copyFile53);
          char *moveFile55[] = {"mv", fileSources2, destination2, NULL};
          createProcess(keyMove, moveFile55);
        }
      }
    }
    (void)closedir(dp);
  }
  else
  {
    exit(0);
  }
}
```

Hasil dari pengerjaan function `moveFileByCategory()` adalah sebagai berikut:

![Img hasil moveFileByCategory()](./)

setelah memindahkan setiap file, kemudian kita akan me-rename setiap file yang ada di folder per-categorynya serta membuat file `data.txt` untuk setiap folder tersebut. Dalam melakukan hal ini, kita mengawalinya dengan menggunakan function `getListFolderDrakor()`. Isi dari function tersebut berguna untuk melakukan list semua isi dari folder `/home/[user]/shift2/drakor/`. Untuk setiap iterasi dalam folder tersebut, kita kemudian akan memanggil function `renameFileAndCreateTxtPerFolder()` dengan menyertakan nama folder hasil iterasi sekarang pada argumentnya. Di dalam function `renameFileAndCreateTxtPerFolder()` kita melakukan list file yang ada untuk folder yang spesifik (contoh untuk folder `/home/[user]/shift2/drakor/action`). Untuk setiap filenya, kita akan menjalanakan fungsi `strtok()` dengan pemisah `;`, untuk file yang hasil katanya ada 3, maka akan direname sesuai dengan kata pertamanya (nama drakor). Lalu data nama dan tahun (kata kedua) akan disimpan pada sebuah array di dalam fungsi `renameFileAndCreateTxtPerFolder()` ini. Sedangkan untuk file yang hasil katanya ada 5, maka akan ditentukan terlebih dahulu apakah kata ketiga / kelimanya sesuai dengan nama folder dimana ia berada sekarang lalu kemudian akan direname. Kita juga akan menyimpan nama file dan nama tahunnya kedalam sebuah array. Setelah perulangan dalam function `renameFileAndCreateTxtPerFolder()` selesai, kita kemudian akan memanggil fungsi `insertionSort()` yang akan melakukan sorting secara ascending untuk array namaFile dan tahun yang dibuat sebelumnya. Setelah array di-sort, kemudian kita sisa melakukan print pada file `data.txt`. Sesuai dengan format yang ditentukan. 

Isi dari function `getListFolderDrakor()` adalah sebagai berikut:

```c
void getListFolderDrakor()
{
  DIR *dp;
  struct dirent *ep;

  dp = opendir("shift2/drakor/");

  if (dp != NULL)
  {
    while ((ep = readdir(dp)))
    {
      if (strcmp(ep->d_name, ".") != 0 && strcmp(ep->d_name, "..") != 0)
      {
        renameFileAndCreateTxtPerFolder(ep->d_name);
      }
    }
    (void)closedir(dp);
  }
  else
  {
    exit(0);
  }
}
```

Isi function `renameFileAndCreateTxtPerFolder()` adalah sebagai berikut:

```c
void renameFileAndCreateTxtPerFolder(char *whereFolder)
{
  DIR *dp;
  struct dirent *ep;
  char path[100] = "shift2/drakor/";
  strcat(path, whereFolder);

  dp = opendir(path);

  if (dp != NULL)
  {
    int arrTahun[5][1] = {};
    char arrFileName[5][100] = {};
    int idx = 0;

    while ((ep = readdir(dp)))
    {
      if (strcmp(ep->d_name, ".") != 0 && strcmp(ep->d_name, "..") != 0)
      {
        int i = 0;
        char arr[5][50] = {};
        char fileName[100];
        char backupFilename[100];
        strcpy(fileName, ep->d_name);
        strcpy(backupFilename, ep->d_name);
        char *token = strtok(ep->d_name, ";");

        while (token != NULL)
        {
          strcpy(arr[i], token);
          i++;
          token = strtok(NULL, ";");
        }

        if (i == 3)
        {
          char corePath[100] = "/home/ramammurshal/shift2/drakor/";
          strcat(corePath, whereFolder);
          strcat(corePath, "/");

          char destination[100];
          char fileSources[100];

          // copy the corePath
          strcpy(destination, corePath);
          strcpy(fileSources, corePath);

          // define file sources
          strcat(fileSources, fileName);

          // define destination
          strcat(destination, arr[0]);
          strcat(destination, ".png");

          arrTahun[idx][0] = atoi(arr[1]);
          strcpy(arrFileName[idx], arr[0]);
          idx++;

          char *moveFile3[] = {"mv", fileSources, destination, NULL};
          createProcess(keyMove, moveFile3);
        }
        else if (i == 5)
        {
          char corePath[100] = "/home/ramammurshal/shift2/drakor/";
          strcat(corePath, whereFolder);
          strcat(corePath, "/");

          char destination[100];
          char fileSources[100];

          // copy the corePath
          strcpy(destination, corePath);
          strcpy(fileSources, corePath);

          // define file sources
          strcat(fileSources, fileName);

          char *file53 = strtok(arr[2], "_");
          char *file55 = strtok(arr[4], ".");

          if (strcmp(file53, whereFolder) == 0)
          {
            // define destination
            strcat(destination, arr[0]);
            strcat(destination, ".png");

            arrTahun[idx][0] = atoi(arr[1]);
            strcpy(arrFileName[idx], arr[0]);
            idx++;
          }
          else if (strcmp(file55, whereFolder) == 0)
          {
            char *token52 = strtok(backupFilename, "_");
            char *token522 = strtok(token52, ";");

            // define destination
            strcat(destination, token522);
            strcat(destination, ".png");

            arrTahun[idx][0] = atoi(arr[3]);
            strcpy(arrFileName[idx], token522);
            idx++;
          }

          char *moveFile[] = {"mv", fileSources, destination, NULL};
          createProcess(keyMove, moveFile);
        }
      }
    }
    // while ((ep = readdir(dp)))
    // {
    //   if (strcmp(ep->d_name, ".") != 0 && strcmp(ep->d_name, "..") != 0)
    //   {
    //     // printf("%s \n", ep->d_name);
    //   }
    // }

    strcat(path, "/data.txt");
    char *createTxt[] = {"touch", path, NULL};
    createProcess(keyTouch, createTxt);

    FILE *f = fopen(path, "w");
    if (f == NULL)
    {
      printf("Error when opening file %s \n", path);
      exit(1);
    }

    fprintf(f, "kategori: %s \n\n", whereFolder);

    insertionSort(arrTahun, arrFileName, 5);

    for (int i = 0; i < 5; i++)
    {
      if (arrTahun[i][0] != 0)
      {
        fprintf(f, "nama: %s \n", arrFileName[i]);
        fprintf(f, "rilis: tahun %d \n\n", arrTahun[i][0]);
      }
    }
    (void)closedir(dp);
  }
  else
  {
    exit(0);
  }
}
```

Isi dari function `insertionSort()` adalah sebagai berikut:

```c
void insertionSort(int arrTahun[5][1], char arrFilename[5][100], int n)
{
  int i, key, j;
  char kuy[100];
  for (i = 1; i < n; i++)
  {
    key = arrTahun[i][0];
    strcpy(kuy, arrFilename[i]);
    j = i - 1;

    while (j >= 0 && arrTahun[j][0] > key)
    {
      arrTahun[j + 1][0] = arrTahun[j][0];
      strcpy(arrFilename[j + 1], arrFilename[j]);
      j = j - 1;
    }
    arrTahun[j + 1][0] = key;
    strcpy(arrFilename[j + 1], kuy);
  }
}
```

Hasil pengerjaan untuk nomor 2.c, 2.d, dan 2.e adalah sebagai gambar berikut:

![Hasil 2.c, 2.d, dan 2.e](./images/2c2d2e1.PNG)

![Hasil 2.c, 2.d, dan 2.e](./images/2c2d2e2.PNG)

## Soal 3
Conan adalah seorang detektif terkenal. Suatu hari, Conan menerima beberapa laporan tentang hewan di kebun binatang yang tiba-tiba hilang. Karena jenis-jenis hewan yang hilang banyak, maka perlu melakukan klasifikasi hewan apa saja yang hilang

### Keterangan Soal 3
- Tidak boleh memakai system().
- Tidak boleh memakai function C mkdir() ataupun rename().
- Gunakan exec dan fork
- Direktori “.” dan “..” tidak termasuk

### Penjabaran Umum Pengerjaan Soal 3
File `soal3.c` yang kelompok kami buat berada dalam folder `/home/[user]/modul2/`. Sedagkan file `animal.zip` juga berada dalam folder tersebut. Di awal file `soal3.c`, pertama-tama kita mendefinisikan header-header yang dibutuhkan terlebih dahulu seperti berikut:

```c
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <wait.h>
#include <dirent.h>
#include <stdio.h>
#include <sys/stat.h>
#include <pwd.h>
#include <grp.h>
```

Setelah itu, kita membuat sebuah variable string global yang akan berguna saat kita menggunakan fungsi `execv()`. String global tersebut sesuai dengan:

```c
char keyMkdir[] = "/bin/mkdir";
char keyUnzip[] = "/usr/bin/unzip";
char keyRemove[] = "/usr/bin/rm";
char keyCopy[] = "/usr/bin/cp";
char keyMove[] = "/usr/bin/mv";
char keyTouch[] = "/usr/bin/touch";
```

Lalu, di dalam function main yang kita miliki, kita membagi setiap process pengerjaan sub-soal ke dalam function-function yang berbeda. Isi dari function `main()` kita ditunjukkan sebagai berikut:

```c
pid_t child_id;
  child_id = fork();
  int status;

  if (child_id < 0)
  {
    exit(EXIT_FAILURE);
  }

  if (child_id != 0)
  {
    createFolderDarat();
  }
  else
  {
    while (wait(&status) > 0)
      ;
    sleep(3);
    createFolderAir();
    unzipAnimalZip();
    moveAnimalFile();
    removeBirdFromDarat();
    createTxt("darat/list.txt");
    writeTxt("darat", "darat/list.txt");
    createTxt("air/list.txt");
    writeTxt("air", "air/list.txt");
  }
```

Setiap function-function yang ada di dalam `main()` tersebut (contoh `moveAnimalFile()`) akan memanggil sebuah function `createProcess()` yang akan berguna untuk membuat process (forking). Isi dari function `createProcess()` adalah seperti berikut:

```c
void createProcess(char *str, char *argv[])
{
  pid_t child_id;
  child_id = fork();
  int status;

  if (child_id == 0)
  {
    execv(str, argv);
  }
  else
  {
    while (wait(&status) > 0)
      ;
  }
}
```

### Soal 3.a
Untuk mempercepat klasifikasi, Conan diminta membuat program untuk membuat 2 directory di “/home/[USER]/modul2/” dengan nama “darat” lalu 3 detik kemudian membuat directory ke 2 dengan nama “air”.

### Pembahasan Soal 3.a
Untuk mengerjakan soal nomor 3.a, kita langsung ke function `main()`. Di mana, di dalam function tersebut kita langsung membuat fork process. Untuk membuat folder darat, kita dapat memanggil function `createFolderDarat()`. Proses pembuatan folder darat ini berada pada parent process. Isi dari function `createFolderDarat()` adalah sebagai berikut:

```c
void createFolderDarat()
{
  char *argv[] = {"mkdir", "darat", NULL};
  createProcess(keyMkdir, argv);
}
```

Setelah itu, kita melakukan wait menggunakan perintah `while (wait(&status) > 0);` sehingga folder darat tersebut dibuat terlebih dahulu. Kita juga menggunakan perintah `sleep(3)` untuk menunggu selama 3 detik setelah folder darat berhasil dibuat. Setelah itu, pada child process kita sisa memanggil function `createFolderAir()` untuk membuat folder air. Isi dari function `createFolderAir()` adalah ditunjukkan sebagai berikut:

```c
void createFolderAir()
{
  char *argv[] = {"mkdir", "air", NULL};
  createProcess(keyMkdir, argv);
}
```

Hasil dari pengerjaan soal 3.a adalah sesuai dengan gambar berikut:

![Hasil 3.a](./images/3a1.PNG)

![Hasil 3.a](./images/3a2.PNG)

### Soal 3.b
Kemudian program diminta dapat melakukan extract “animal.zip” di “/home/[USER]/modul2/”

### Pembahasan Soal 3.b
Dalam mengerjakan soal 3.b, kita sisa memanggil function `unzipAnimalZip()` di dalam child process sebelumnya. Isi dari function `unzipAnimalZip()` adalah sebagai berikut:

```c
void unzipAnimalZip()
{
  char *argv[] = {"unzip", "-qo", "animal.zip", NULL};
  createProcess(keyUnzip, argv);
}
```

Hasil dari pengerjaan soal 3.b adalah sesuai dengan gambar berikut:

![Hasil 3.b](./images/3b.PNG)

### Soal 3.c
Tidak hanya itu, hasil extract dipisah menjadi hewan darat dan hewan air sesuai dengan nama filenya. Untuk hewan darat dimasukkan ke folder “/home/[USER]/modul2/darat” dan untuk hewan air dimasukkan ke folder “/home/[USER]/modul2/air”. Rentang pembuatan antara folder darat dengan folder air adalah 3 detik dimana folder darat dibuat terlebih dahulu. Untuk hewan yang tidak ada keterangan air atau darat harus dihapus.

### Pembahasan Soal 3.c
Dalam mengerjakan soal 3.c, kita cukup memanggil fungsi `moveAnimalFile()`. Isi dari function tersebut adalah melakukan list semua file yang ada di folder animal (hasil extract `animal.zip`). Untuk setiap iterasi per-nama filenya, kita menenukan apakah ia mengandung karakter `darat` atau karakter `air`. Dalam menentukan hal tersebut kita menggunakan function `strstr()` bawaan c. Kemudian, kita sisa memindahkannya sesuai karakter yang ia kandung. Untuk file yang namanya tidak mengandung keduanya akan dihapus. Isi total dari function `moveAnimalFile()` adalah sebagai berikut:

```c
void moveAnimalFile()
{
  DIR *dp;
  struct dirent *ep;
  pid_t childs;
  int status = 0;

  childs = fork();

  if (childs == 0)
  {
    dp = opendir("animal");
    if (dp != NULL)
    {
      while ((ep = readdir(dp)))
      {
        if (strcmp(ep->d_name, ".") != 0 && strcmp(ep->d_name, "..") != 0)
        {
          char *containDarat = strstr(ep->d_name, "darat");

          char source[100] = "animal/";
          strcat(source, ep->d_name);

          if (containDarat)
          {
            moveFile("darat", source);
          }
        }
      }
      (void)closedir(dp);
    }
  }
  else
  {
    while (wait(&status) > 0)
      ;
    sleep(3);
    dp = opendir("animal");
    if (dp != NULL)
    {
      while ((ep = readdir(dp)))
      {
        if (strcmp(ep->d_name, ".") != 0 && strcmp(ep->d_name, "..") != 0)
        {
          char *containAir = strstr(ep->d_name, "air");

          char source[100] = "animal/";
          strcat(source, ep->d_name);

          if (containAir)
          {
            moveFile("air", source);
          }
          else
          {
            removeFile(source);
          }
        }
      }
      (void)closedir(dp);
    }
  }
}
```

Hasil pengerjaan 3.c adalah sebagai berikut:

![hasil 3.c](./images/3c.PNG)

### Soal 3.d
Setelah berhasil memisahkan hewan berdasarkan hewan darat atau hewan air. Dikarenakan jumlah burung yang ada di kebun binatang terlalu banyak, maka pihak kebun binatang harus merelakannya sehingga conan harus menghapus semua burung yang ada di directory “/home/[USER]/modul2/darat”. Hewan burung ditandai dengan adanya “bird” pada nama file.

### Pembahasan Soal 3.d
Dalam mengerjakan soal 3.d, kita cukup memanggil function `removeBirdFromDarat()`. Isi dari function tersebut adalah melakukan listing file yang ada di dalam folder `“/home/[USER]/modul2/darat`. Lalu, untuk setiap nama file yang mengandung karakter `darat`, akan dihapus. Dalam menentukannya, kita menggunakan function `strstr()` bawaan C. Isi dari function `removeBirdFromDarat()` adalah sebagai berikut:

```c
void removeBirdFromDarat()
{
  DIR *dp;
  struct dirent *ep;

  dp = opendir("darat");

  if (dp != NULL)
  {
    while ((ep = readdir(dp)))
    {
      if (strcmp(ep->d_name, ".") != 0 && strcmp(ep->d_name, "..") != 0)
      {
        char *containBird = strstr(ep->d_name, "bird");

        char source[100] = "darat/";
        strcat(source, ep->d_name);

        if (containBird)
        {
          removeFile(source);
        }
      }
    }

    (void)closedir(dp);
  }
  else
  {
    exit(0);
  }
}
```

Hasil pengerjaan 3.d adalah sebagai berikut:

![hasil 3.d](./images/3d.PNG)

### Soal 3.e
Terakhir, Conan harus membuat file list.txt di folder “/home/[USER]/modul2/air” dan membuat list nama semua hewan yang ada di directory “/home/[USER]/modul2/air” ke “list.txt” dengan format UID_[UID file permission]_Nama File.[jpg/png] dimana UID adalah user dari file tersebut file permission adalah permission dari file tersebut. Contoh : conan_rwx_hewan.png

### Pembahasan Soal 3.e
Dalam mengerjakan soal 3.e, kita menggunakan 2 function yang saling terkait. Function pertama adalah `createTxt()`, yang berguna untuk membuat file txt di dalam folder darat atau air. Kita sisa passing path dimana ingin membuat file txt tersebut pada argumennya. Contoh: `createTxt("darat/list.txt")`. Isi dari function tersebut adalah sebagai berikut:

```c
void createTxt(char *where)
{
  char *argv[] = {"touch", where, NULL};
  createProcess(keyTouch, argv);
}
```

Setelah itu, kita memanggil function `writeTxt()` untuk menulis isi txt yang sudah dibuat. Kita passing folder apa, serta source filenya. Contoh: `writeTxt("darat", "darat/list.txt")`. Isi dari function tersebut adalah pertama melakukan listing file sesuai folder parameter pertamanya. Lalu untuk setiap iterasi nama filenya, kita sisa ambil data akses dari file menggunakan template dari github serta nama user pemilik filenya yang juga sesuai dengan kode template di github. Kemudia print ke path txt yang sesuai dengan parameter keduanya. Isi dari function `writeTxt()` adalah sesuai berikut:

```c
void writeTxt(char *folder, char *file)
{
  // dir
  DIR *dp;
  struct dirent *ep;
  dp = opendir(folder);

  // file
  FILE *f = fopen(file, "w");
  if (f == NULL)
  {
    exit(1);
  }
  struct stat info;
  struct stat fs;
  int r;
  int s;

  if (dp != NULL)
  {
    while ((ep = readdir(dp)))
    {
      if (strcmp(ep->d_name, ".") != 0 && strcmp(ep->d_name, "..") != 0)
      {
        char temp[999];
        char temp2[999];
        if (folder == "darat")
        {
          strcpy(temp, "darat/");
          strcpy(temp2, "darat/");
        }
        else
        {
          strcpy(temp, "air/");
          strcpy(temp2, "air/");
        }
        r = stat(strcat(temp, ep->d_name), &info);
        s = stat(strcat(temp2, ep->d_name), &fs);

        if (r == -1 || s == -1)
        {
          fprintf(stderr, "File error satu \n");
          exit(1);
        }

        struct passwd *pw = getpwuid(info.st_uid);
        if (pw == 0)
        {
          fprintf(stderr, "File error dua \n");
          exit(1);
        }
        char *userFile = pw->pw_name;
        fprintf(f, "%s_", userFile);

        if (fs.st_mode & S_IRUSR)
        {
          fprintf(f, "r");
        }
        else
        {
          fprintf(f, "-");
        }

        if (fs.st_mode & S_IWUSR)
        {
          fprintf(f, "w");
        }
        else
        {
          fprintf(f, "-");
        }

        if (fs.st_mode & S_IXUSR)
        {
          fprintf(f, "x");
        }
        else
        {
          fprintf(f, "-");
        }

        char *fileName = ep->d_name;
        fprintf(f, "_%s \n", fileName);
      }
    }

    fclose(f);
    (void)closedir(dp);
  }
  else
  {
    exit(0);
  }
}
```

Hasil pengerjaan 3.e adalah sebagai berikut:

![hasil 3.e](./images/3e1.PNG)

![hasil 3.e](./images/3e2.PNG)

![hasil 3.e](./images/3e3.PNG)
