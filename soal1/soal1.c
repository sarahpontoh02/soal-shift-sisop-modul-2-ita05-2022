#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <wait.h>
#include <dirent.h>
#include <stdio.h>
#include <sys/stat.h>
#include <pwd.h>
#include <grp.h>
#include <json-c/json.h>
#include <time.h>
#include <syslog.h>
#include <fcntl.h>


char keyMkdir[] = "/bin/mkdir";
char keyRmove[] = "/usr/bin/rm";
char keyZip[] = "/usr/bin/zip";
char keyUnzip[] = "/usr/bin/unzip";
char keyWget[] = "/usr/bin/wget";

void createProcess(char *str, char *argv[])
{
  pid_t child_id;
  child_id = fork();
  int status;

  if (child_id == 0)
  {
    execv(str, argv);
  }
  else
  {
    while (wait(&status) > 0)
      ;
  }
}




void download(char *db, char *file)
{
  char *argv[] = {"wget", "--no-check-certificate", "-q", db, "-O", file, NULL}; 
  createProcess(keyWget, argv);
}

void unzip(char *file)
{
  char *argv[] = {"unzip", "-q", "-n", file, NULL}; 
  createProcess(keyUnzip, argv);
}

void createFolderGacha(char *dir)
{
  DIR *checkDir = opendir(dir);
  if (checkDir)
  { //check if gacha_gacha exists
    closedir(checkDir);
  }
  else
  {
    char *argv[] = {"mkdir", dir, NULL}; 
    createProcess(keyMkdir, argv);
  }
}



int countItem(char *basePath)
{
  DIR *d;
  struct dirent *dir;
  d = opendir(basePath);

  //count number of json files
  int count = 0;
  if (d)
  {
    while ((dir = readdir(d)) != NULL)
    {
      //printf("%s\n", dir->d_name);
      count++;
    }
  }

  return count;
}

//pick random item(json file) from folder
char *pickItem(char *basePath)
{
  DIR *d;
  struct dirent *dir;
  d = opendir(basePath);

  //count how many json files
  int files = countItem(basePath);
  //generate random number
  int random = rand()%(files-1) + 1;

  if (d)
  {
    int i=0;
    while ((dir = readdir(d)) != NULL)
    {
      if(i>=random-1) 
      {
        return dir->d_name;
      }
      i++;
    }
  }
  
}

//get the name and rarity from an item
char *fetch(char *basePath)
{
  FILE *fp;
	char buffer[4096];
  char *data = malloc (sizeof (char) * 1024);
	struct json_object *parsed_json;
	struct json_object *name;
	struct json_object *rarity;

  char filename[100];
  snprintf(filename, 100, "%s/%s", basePath, pickItem(basePath));


  fp = fopen(basePath,"r");
	fread(buffer, 4096, 1, fp);
	fclose(fp);

	parsed_json = json_tokener_parse(buffer);

	json_object_object_get_ex(parsed_json, "name", &name);
	json_object_object_get_ex(parsed_json, "rarity", &rarity);

  snprintf(data, 1024, "%d_%s", json_object_get_int(rarity), json_object_get_string(name));
  return data;
}

#define primogems 79000
#define cost 160

void gacha()
{
  char *waktu;
  char *filename; 
  char *foldername; 
  char *message;
  int current_primogems = primogems;
  int gacha_number = 1;
  FILE * fptr;
  while(current_primogems > cost)
  {
    current_primogems-=cost;
    if((gacha_number%90-1)==0)
    {
      if(gacha_number > 1) chdir("..");
      snprintf(foldername, 100, "total_gatcha_%d", gacha_number);

      char *argv[] = {"mkdir", "-p", foldername, NULL};
      createProcess(keyMkdir, argv);
      chdir(foldername);
    }

    if((gacha_number%10-1)==0 || gacha_number==1)
    {
      sleep(1);
      time_t t1 = time(NULL);
      struct tm* p1 = localtime(&t1);

      strftime(waktu, 30, "%H:%M:%S", p1);
      snprintf(filename, 100, "%s_gatcha_%d.txt", waktu, gacha_number);
    }

    if(gacha_number%2==0)
    {
      fptr = fopen(filename, "a+");
      snprintf(message, 100, "%d_weapon_%s_%d\n", gacha_number, fetch("../../weapons"), current_primogems);
      fputs(message, fptr);
      fclose(fptr);
    }
    else
    {
      fptr = fopen(filename, "a+");
      snprintf(message, 100, "%d_character_%s_%d\n", gacha_number, fetch("../../characters"), current_primogems);
      fputs(message, fptr);
      fclose(fptr);
    }

    gacha_number++;

  }

  chdir("..");

}

void private()
{
  
  char *argv[] = {"zip", "--password", "satuduatiga", "-r", "not_safe_for_wibu.zip", "gacha_gacha", NULL};
  createProcess(keyZip, argv);

  char *argv2[] = {"rm", "-r", "gacha_gacha", "characters", "weapons", NULL};
  createProcess(keyRmove, argv2);

  char *argv3[] = {"rm", "characters.zip", "weapons.zip", NULL};
  createProcess(keyRmove, argv3);

}



int main() 
{
	pid_t pid, sid;

  pid = fork();

  if (pid < 0) 
  {
    exit(EXIT_FAILURE);
  }

  if (pid > 0) 
  {
    exit(EXIT_SUCCESS);
  }

  umask(0);

  sid = setsid();
  if (sid < 0) 
  {
    exit(EXIT_FAILURE);
  }

  if ((chdir("/home/sarahhpth/sisop/soal-shift-sisop-modul-2-ita05-2022/soal1")) < 0) 
  {
    exit(EXIT_FAILURE);
  }

  close(STDIN_FILENO);
  close(STDOUT_FILENO);
  close(STDERR_FILENO);

  

  while(1)
  {
    char *db[2] = 
    {
      "https://drive.google.com/u/0/uc?id=1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp&export=download",
      "https://drive.google.com/u/0/uc?id=1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT&export=download"
    };
    char *file[2] = 
    {
      "characters.zip", 
      "weapons.zip"
    };
    char *folder[1] = {"gacha_gacha"};
    int gacha_num = 0;

    download(db[0], file[0]);
    download(db[1], file[1]);
    unzip(file[0]);
    unzip(file[1]);
    createFolderGacha(folder[0]);
      

    gacha();
    private();
    
  }
  return 0;

  
}